const intercept = (obj, interceptionFn) => {  
    const handler = { 
	    get(target, property) {  	
         if ((Reflect.has(target, property)) && 
         (target[property] instanceof Function)) { 
            interceptionFn.call(target, property);  
        }
        return Reflect.get(target, property);  },
};
return new Proxy(obj, handler);  
};

const toBeIntercepted = {  name: 'Jonathan', getName() {	return this.name;  },
  setName(name) {	this.name = name;}, };

  const logger = () => {console.log('logger called')}

  const proxied = intercept(toBeIntercepted, logger);
  console.log(proxied.getName())


