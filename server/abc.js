class Person {  
    constructor(name) { 
	this.name = name;
  }
    getName() {  return this.name;}
    sayHello(to) { 
        return `Hello ${to.getName()}. My name is ${this.name}.`;
    } 
}

class SuperHero extends Person {
    constructor(name, superpower) {
        super(name);
        this.superpower = superpower; 
        SuperHero.internalCount += 1;
     }
  static count() { return SuperHero.internalCount;} 
}
  
SuperHero.internalCount = 0; 

const thor = new SuperHero('Thor', 'Mjolnir');
const blackWidow = new SuperHero('Black Widow', 'weapons specialist')

console.log(SuperHero.count())
